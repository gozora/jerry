Function Set-WindowSize {
Param([int]$x=$host.ui.rawui.windowsize.width,
      [int]$y=$host.ui.rawui.windowsize.heigth)

    $size=New-Object System.Management.Automation.Host.Size($x,$y)
    $host.ui.rawui.WindowSize=$size   
}

function reset {
    Set-Buffer-Width-To-Screen-Width
    Clear-Host
}

function Set-Buffer-Width-To-Screen-Width {
    $h = Get-Host
    $ui = $h.UI.RawUI
    $bufferSize = $ui.BufferSize
    $windowSize = $ui.WindowSize
    $bufferSize.Width = $windowSize.Width
    $ui.BufferSize = $bufferSize
}

Set-WindowSize 60 10
reset
Set-Buffer-Width-To-Screen-Width

Start-Process -NoNewWindow -FilePath "C:\<path_to_jerry>\jerry-windows-amd64.exe"
# Start-Process -NoNewWindow -FilePath "C:\<path_to_jerry>\jerry-windows-amd64.exe" -ArgumentList "--timeout 2m"
