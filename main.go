package main

import (
	"fmt"
	"time"
	"flag"
	"log"
	rg "github.com/go-vgo/robotgo"
)

func isFlagSet(name string) bool {
	var found bool = false

	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})

	return found
}

func moveMouse(x, y int, t *time.Ticker) {
	for range t.C {
		rg.MoveSmoothRelative(x, y)
		x *= -1
		y *= -1

		log.Println("Mouse pointer moved.")
	}
}


func main() {
	const NAME string = "jerry"
	const MAJOR int = 1
	const MINOR int = 0
	const PATCH int = 0

	var version string = fmt.Sprintf("%d.%d.%d", MAJOR, MINOR, PATCH)
	var nameVersion string = fmt.Sprintf("%s-%d.%d.%d", 
		NAME, MAJOR, MINOR, PATCH)

	/*
	 *	BEGIN of parse command line arguments (flags)
	 */
	timeout := flag.Duration("timeout", 2 * time.Minute, 
		"Mouse inactivity timeout. e.g. 5s, 10m, ... [>= 5s]")

	// -move 20 is shortcut for -x 20 -y 20
	move := flag.Int("move", 2, "Move mouse on X and Y axis by number " + 
		"of pixels. If set, overrides -x and -y.")

	moveX := flag.Int("x", 2, "Move mouse on X axis by number of pixels.")
	moveY := flag.Int("y", 2, "Move mouse on Y axis by number of pixels.")
	showVersion := flag.Bool("version", false, "Show version info.")
	showVersionShort := flag.Bool("V", false, "Show version info.")
	
	flag.Parse()

	if isFlagSet("move") {
		moveX = move
		moveY = move
	}

	if *timeout < 5 * time.Second {
		log.Fatal("Timeout value too small.")
	}

	if *showVersion || *showVersionShort {
		fmt.Println(nameVersion)
		return
	}
	/*
	 *	END of parse command line arguments
	 */

	log.Printf("Starting %s-%s [timeout=%v]\n", NAME, version, timeout)

	// go-routine will run in background and try to move mouse every timeout,
	// main process will reset ticker when mouse changes position within 1 s.
	ticker := time.NewTicker(*timeout)
	defer ticker.Stop()

	go moveMouse(*moveX, *moveY, ticker)

	var prevX, prevY int
	for i := 0; true; i++ {
		x, y := rg.GetMousePos()
		
		if x != prevX || y != prevY {
			ticker.Reset(*timeout)
		}

		prevX, prevY = x, y

		time.Sleep(1000 * time.Millisecond)
	}
}
