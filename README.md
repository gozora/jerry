# Jerry

Automatically moves mouse pointer when inactive for more than timeout specified period of time. I wrote this program to avoid my work laptop, where inactivity timeout is enforced by corporate domain rules, to lock.

## TODO
- [ ] compilation instructions

## Installation
### Compile from source
TBD

### Download packages
You can download packages in [package registry](https://gitlab.com/gozora/jerry/-/packages).

## Usage
```
Usage of ./jerry:
  -V	Show version info.
  -move int
    	Move mouse on X and Y axis by number of pixels. If set, overrides -x and -y. (default 2)
  -timeout duration
    	Mouse inactivity timeout. e.g. 5s, 10m, ... [>= 5s] (default 2m0s)
  -version
    	Show version info.
  -x int
    	Move mouse on X axis by number of pixels. (default 2)
  -y int
    	Move mouse on Y axis by number of pixels. (default 2)

```

## Support
*Jerry* is quite simple so I don't expect any trouble with its usage. If you despite feel like you need help, you can open new [issue](https://gitlab.com/gozora/jerry/-/issues/new).

## Roadmap
I don't plan to add any new features or build for other platforms. Should you badly need binary for other than already provided platforms, open new [issue](https://gitlab.com/gozora/jerry/-/issues/new) and I'll try to help.

## Contributing
I'm not accepting any merge requests.

## License
Jerry is released under [MIT](https://gitlab.com/gozora/jerry/-/blob/main/LICENSE) license.


